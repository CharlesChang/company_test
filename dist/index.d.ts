/// <reference types="express" />
import * as express from 'express';
export declare class Server {
    private readonly PORT;
    private _app;
    constructor();
    readonly app: express.Application;
    private bootstrap();
    private execute();
}
export declare const server: Server;

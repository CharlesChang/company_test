"use strict";
const env = process.env.NODE_ENV || "development"; // production environment is usually set by the server itself
console.log('\x1b[32m%s\x1b[0m', `NODE_ENV = ${env}`);
if (env === "development") {
    process.env.PORT = "4000";
    process.env.MONGODB_URI = "mongodb://localhost:27017/dev";
}
else if (env === "test") {
    process.env.PORT = "3000";
    process.env.MONGODB_URI = "mongodb://localhost:27017/test";
}
//# sourceMappingURL=envConfig.js.map
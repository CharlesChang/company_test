/// <reference types="express" />
import { Application } from "express";
export declare class RouteCreator {
    constructor(app: Application);
    private assembleRoutes(app);
}
export declare const routeCreator: (app: Application) => RouteCreator;

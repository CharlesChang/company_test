/// <reference types="express" />
import { Application } from 'express';
export declare class DepartmentAPI {
    constructor(app: Application);
}
export declare const departmentAPI: (app: Application) => DepartmentAPI;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const database_module_1 = require("../mongodb/database.module");
class DepartmentAPI {
    constructor(app) {
        //find all
        app.get("/departments", (req, rsp) => {
            database_module_1.departmentDB.findAllDepartment().then(data => {
                rsp.status(200)
                    .send(data);
            }, err => {
                rsp.status(403).send(err);
            });
        });
        //find by id
        app.get('/departments/:id', (req, rsp) => {
            console.log("id = ", req.params.id);
            database_module_1.departmentDB.findDepartmentById(req.params.id).then(data => {
                rsp.status(200)
                    .send(data);
            }, err => {
                rsp.status(403)
                    .send(err);
            });
        });
        //create
        app.post("/departments", (req, rsp) => {
            let { id, name } = req.body;
            database_module_1.departmentDB.createNewDepartment({ id, name })
                .then(newDep => {
                rsp.status(200)
                    .send(newDep);
            }, err => {
                rsp.status(403).send(err);
            });
        });
        //update
        app.put("/departments", (req, rsp) => {
            let { id, name } = req.body;
            database_module_1.departmentDB.findOneAndUpdate({ id, name })
                .then(newUser => {
                rsp.status(200)
                    .send(newUser);
            }, err => {
                rsp.status(403).send(err);
            });
        });
        //delete
        app.delete("/departments/:id", (req, rsp) => {
            database_module_1.departmentDB.findOneAndDelete(req.params.id).then(success => {
                rsp.status(200).send(success);
            }, err => {
                rsp.status(403).send(err);
            });
        });
        // your new routes here
    }
}
exports.DepartmentAPI = DepartmentAPI;
exports.departmentAPI = (app) => new DepartmentAPI(app);
//# sourceMappingURL=department.api.js.map
/// <reference types="express" />
import { Application } from 'express';
export declare class EmployeeAPI {
    constructor(app: Application);
}
export declare const employeeAPI: (app: Application) => EmployeeAPI;

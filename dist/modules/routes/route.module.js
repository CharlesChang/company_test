"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const department_api_1 = require("./department.api");
const employee_api_1 = require("./employee.api");
const bodyParser = require("body-parser");
const cors = require("cors");
class RouteCreator {
    constructor(app) {
        // routing configuration
        app.use(cors());
        app.use(bodyParser.json());
        // attach all routes
        this.assembleRoutes(app);
    }
    assembleRoutes(app) {
        // Add more modularized routes
        employee_api_1.employeeAPI(app);
        department_api_1.departmentAPI(app);
    }
}
exports.RouteCreator = RouteCreator;
exports.routeCreator = (app) => new RouteCreator(app);
//# sourceMappingURL=route.module.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const database_module_1 = require("../mongodb/database.module");
class EmployeeAPI {
    constructor(app) {
        //Gets all employees in the table
        app.get("/employees", (req, rsp) => {
            database_module_1.employeeDB.findAllEmployee().then(data => {
                rsp.status(200)
                    .send(data);
            }, err => {
                rsp.status(500).send(err);
            });
        });
        app.get('/employees/:id', (req, rsp) => {
            console.log("id = ", req.params.id);
            database_module_1.employeeDB.findEmployeeById(req.params.id).then(data => {
                rsp.status(200)
                    .send(data);
            }, err => {
                rsp.status(403)
                    .send(err);
            });
        });
        app.post("/employees", (req, rsp) => {
            let { id, name, age } = req.body;
            database_module_1.employeeDB.createNewEmployee({ id, name, age })
                .then(newUser => {
                rsp.status(200)
                    .send(newUser);
            }, err => {
                rsp.status(403).send(err);
            });
        });
        app.put("/employees", (req, rsp) => {
            let { id, name, age } = req.body;
            database_module_1.employeeDB.findOneAndUpdate({ id, name, age })
                .then(newUser => {
                rsp.status(200)
                    .send(newUser);
            }, err => {
                rsp.status(403).send(err);
            });
        });
        app.delete("/employees/:id", (req, rsp) => {
            database_module_1.employeeDB.findOneAndDelete(req.params.id).then(success => {
                rsp.status(200).send(success);
            }, err => {
                rsp.status(403).send(err);
            });
        });
        //get all employees in a department
        app.get("/departments/:dpid/employees", (req, rsp) => {
            database_module_1.employeeDB.findEmployeeByDepartmentId(req.params.dpid).then(data => {
                rsp.status(200).send(data);
            }, err => {
                rsp.status(403).send(err);
            });
        });
        //add employee to department
        app.post("/departments/:dpid/employees/:epid", (req, rsp) => {
            database_module_1.employeeDB.addToDepartment(req.params.dpid, req.params.epid).then(data => {
                rsp.status(200).send(data);
            }, err => {
                rsp.status(403).send(err);
            });
        });
        // your new routes here
    }
}
exports.EmployeeAPI = EmployeeAPI;
exports.employeeAPI = (app) => new EmployeeAPI(app);
//# sourceMappingURL=employee.api.js.map
/// <reference types="mongoose" />
import { DepartmentData } from '../../model/department.model';
import { Model, Document } from "mongoose";
export declare class Department {
    private DepartmentModel;
    private schema;
    constructor();
    /**
     * Try not to use the model directly since the type definition will be lost
     */
    readonly model: Model<Document>;
    findAllDepartment(): Promise<DepartmentData[]>;
    findDepartmentById(id: number): Promise<DepartmentData>;
    createNewDepartment(dep: DepartmentData): Promise<DepartmentData>;
    findOneAndUpdate(dep: DepartmentData): Promise<DepartmentData>;
    findOneAndDelete(id: number): Promise<DepartmentData>;
}
export declare const _department: Department;

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const logger = require('tracer').console();
class Department {
    constructor() {
        this.schema = new mongoose_1.Schema({
            id: {
                type: Number,
                required: true
            },
            name: {
                type: String,
                required: true
            },
        });
        // set up some models
        this.DepartmentModel = mongoose_1.model("Department", this.schema);
    }
    ;
    /**
     * Try not to use the model directly since the type definition will be lost
     */
    get model() {
        return this.DepartmentModel;
    }
    findAllDepartment() {
        return this.DepartmentModel.find();
    }
    findDepartmentById(id) {
        return this.DepartmentModel.findOne({ id: id });
    }
    createNewDepartment(dep) {
        let model = new this.DepartmentModel(dep);
        return model.save();
    }
    findOneAndUpdate(dep) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            try {
                let doc = yield this.DepartmentModel.findOneAndUpdate({
                    id: dep.id
                }, {
                    $set: {
                        name: dep.name,
                    }
                }, {
                    new: true
                });
                resolve(doc);
            }
            catch (err) {
                reject(err);
            }
        }));
    }
    findOneAndDelete(id) {
        return this.DepartmentModel.findOneAndRemove({ id: id });
    }
}
exports.Department = Department;
exports._department = new Department();
//# sourceMappingURL=departments.js.map
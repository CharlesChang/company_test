"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const departments_1 = require("./departments");
const employees_1 = require("./employees");
const mongoose = require("mongoose");
const logger = require('tracer').console();
mongoose.Promise = global.Promise; //using node default promise for mongoose
class MongooseModule {
    constructor() {
        this.database = mongoose.connection;
    }
    bootstrap() {
        let me = this;
        mongoose.connect(process.env.MONGODB_URI, { useMongoClient: true });
        this.database.on('error', logger.error.bind(logger, 'connection error:'));
        this.database.once('open', () => {
            // we're connected!
            me.onCreateHook();
        });
    }
    onCreateHook() {
        return __awaiter(this, void 0, void 0, function* () {
            //Your hook
        });
    }
    closeDatabase() {
        if (this.database) {
            this.database.close();
        }
        else {
            logger.log("Unable to close database: database was not initialized.");
        }
    }
}
exports.MongooseModule = MongooseModule;
exports.mongooseDB = new MongooseModule();
exports.employeeDB = employees_1._employee;
exports.departmentDB = departments_1._department;
/* utilutil */ 
//# sourceMappingURL=database.module.js.map
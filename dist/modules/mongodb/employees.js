"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const logger = require('tracer').console();
class Employee {
    constructor() {
        this.schema = new mongoose_1.Schema({
            id: {
                type: Number,
                required: [true, "id is required"]
            },
            name: {
                type: String,
                required: [true, "name is required"]
            },
            age: {
                type: Number,
                required: [true, "age is required"]
            },
            department: {
                type: Number
            }
        });
        // set up some models
        this.EmployeeModel = mongoose_1.model("Employee", this.schema);
    }
    ;
    /**
     * Try not to use the model directly since the type definition will be lost
     */
    get model() {
        return this.EmployeeModel;
    }
    findAllEmployee() {
        return this.EmployeeModel.find();
    }
    findEmployeeById(id) {
        return this.EmployeeModel.findOne({ id: id });
    }
    createNewEmployee(user) {
        let model = new this.EmployeeModel(user);
        return model.save();
    }
    findOneAndUpdate(user) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            try {
                let doc = yield this.EmployeeModel.findOneAndUpdate({
                    id: user.id
                }, {
                    $set: {
                        name: user.name,
                        age: user.age
                    }
                }, {
                    new: true
                });
                resolve(doc);
            }
            catch (err) {
                reject(err);
            }
        }));
    }
    findOneAndDelete(id) {
        return this.EmployeeModel.findOneAndRemove({ id: id });
    }
    findEmployeeByDepartmentId(dpid) {
        return this.EmployeeModel.find({ department: dpid });
    }
    addToDepartment(dpid, epid) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            try {
                let doc = yield this.EmployeeModel.findOneAndUpdate({
                    id: epid,
                }, {
                    $set: {
                        department: dpid
                    }
                }, {
                    new: true
                });
                resolve(doc);
            }
            catch (err) {
                reject(err);
            }
        }));
    }
}
exports.Employee = Employee;
exports._employee = new Employee();
//# sourceMappingURL=employees.js.map
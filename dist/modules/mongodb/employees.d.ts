/// <reference types="mongoose" />
import { EmployeeData } from '../../model/employee.model';
import { Model, Document } from "mongoose";
export declare class Employee {
    private EmployeeModel;
    private schema;
    constructor();
    /**
     * Try not to use the model directly since the type definition will be lost
     */
    readonly model: Model<Document>;
    findAllEmployee(): Promise<EmployeeData[]>;
    findEmployeeById(id: number): Promise<EmployeeData>;
    createNewEmployee(user: EmployeeData): Promise<EmployeeData>;
    findOneAndUpdate(user: EmployeeData): Promise<EmployeeData>;
    findOneAndDelete(id: number): Promise<EmployeeData>;
    findEmployeeByDepartmentId(dpid: number): Promise<EmployeeData[]>;
    addToDepartment(dpid: number, epid: number): Promise<EmployeeData>;
}
export declare const _employee: Employee;

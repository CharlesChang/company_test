import { Department } from './departments';
import { Employee } from './employees';
export declare class MongooseModule {
    private database;
    constructor();
    bootstrap(): void;
    private onCreateHook();
    closeDatabase(): void;
}
export declare const mongooseDB: MongooseModule;
export declare const employeeDB: Employee;
export declare const departmentDB: Department;

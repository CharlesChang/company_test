export declare type DataBaseName = "Department" | "Employee";
export interface dbRspError {
    errors: any;
    message: string;
}

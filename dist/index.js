"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const route_module_1 = require("./modules/routes/route.module");
/* THIS CONFIGURATION STAYS TOP */
require("./envConfig");
/* END OF ENV CONFIGURATION */
const express = require("express");
const database_module_1 = require("./modules/mongodb/database.module");
const portscanner = require("portscanner");
const logger = require('tracer').console();
class Server {
    constructor() {
        this.PORT = Number(process.env.PORT);
        this.bootstrap();
        this.execute();
    }
    get app() {
        return this._app;
    }
    bootstrap() {
        /*Put all your startup hooks here*/
        this._app = express();
        database_module_1.mongooseDB.bootstrap();
        // find available port
        portscanner.checkPortStatus(this.PORT).then((status) => {
            if (status === "closed") {
                // port available case
                this._app.listen(this.PORT);
                console.log('\x1b[34m%s\x1b[0m', `Listening on port ${this.PORT}`);
            }
            else {
                // port not available case
                portscanner.findAPortNotInUse(3000).then(openPort => {
                    this._app.listen(openPort);
                    console.log('\x1b[34m%s\x1b[0m', `Listening on port ${openPort}`);
                });
            }
        });
    }
    execute() {
        /* Put all your executions here*/
        // extract all routes into a decoupled module
        route_module_1.routeCreator(this._app);
    }
}
exports.Server = Server;
exports.server = new Server();
//# sourceMappingURL=index.js.map
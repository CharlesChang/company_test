## Requirement

1. Please have Nodejs 8.0.0 and above installed
2. Please have Mongo installed
3. Please Have TypeScript installed

## Commands
1. `npm install` to install all the modules
2. `npm run mongo` to start database.
3. `npm run watch-serve` to run the development server on localhost

## Testing
Automatic Testing every end point is avaialble. After mongodb is started,
```npm run watch-test```
 will start up the automatic testing to ensure all end points are running correctly

P.S. The supertest library itself has some bugs that will cause errors such as " call of undefined". This can be solved by changing the library itself. 


## Build for Production
You need to build all TypeScript files into JavaScript into /dist folder first by running

```
tsc
```
TypeScript Compiler Options have already been configured into tsconfig.json

After that, run
`npm run start`
to start the server


## If you don't have TypeScript
you can simply run 
```
npm run mongo
```
```
npm run start
```

And you will start nodejs instance locally

import { departmentDB } from '../src/modules/mongodb/mongodb.module';
const request = require("supertest");
import { server } from "../src/index";
const logger = require('tracer').console();

describe("Department API Testing", () => {
    const dp1 = {
        id: 101,
        name: "IT"
    }
    const dp2 = {
        id: 100,
        name: "Marketing"
    }


    beforeEach(() => {
        return departmentDB.model.remove({})
            .then(() => {
                return departmentDB.createNewDepartment(dp1);
            })
            .then(() => {
                return departmentDB.createNewDepartment(dp2);
            })
    })

    //find all
    it("Should return 2 entries when query for all", done => {
        return request(server.app)
            .get("/departments")
            .expect(200)
            .expect(rsp=>{
                expect(rsp.body.length).toBe(2);
            })
            .end(done());
    })

    //update 
    it("should change the name of department 101 to Math", done => {
        return request(server.app)
            .put("/departments/101")
            .send({ id: 101, name: "Math" })
            .expect(200)
            .expect(rsp => {
                expect(rsp.body.name).toBe("Math")
            })
            .end(done());
    })

    //add new
    it("Should add a new department", done => {
        return request(server.app)
            .post('/departments')
            .send({
                id: 102,
                name: "Engineering"
            })
            .expect(200)
            .expect(rsp => {
                expect(rsp.body.name).toBe("Engineering");
            })
            .then(() => {
                return departmentDB.findAllDepartment()
            })
            .then(rsp => {
                expect(rsp.length).toBe(3);
                done();
            })
    })

    //add new
    it("should not add a new department if name is missing", done=>{
        return request(server.app)
            .post("/departments")
            .send({
                id:102
            })
            .expect(403)
            .end(done());
    });


    //delete
    it("should delete an department", done=>{
        return request(server.app)
            .delete("/departments/101")
            .expect(200)
            .expect(rsp=>{
                expect(rsp.body.name).toBe("IT");
            })
            .then(()=>{
                return departmentDB.findAllDepartment()
            })
            .then(rsp=>{
                expect(rsp.length).toBe(1)
                done();
            })
    })
})
import { employeeDB } from '../src/modules/mongodb/mongodb.module';
const request = require("supertest");
import { server } from "../src/index";
const logger = require('tracer').console();


describe("Employees API Testing", () => {
    const emp1 = {
        id: 1,
        name: "Tom",
        age: 33
    }

    const emp2 = {
        id: 2,
        name: "Siri",
        age: 28
    }


    beforeEach(() => {
        // pitchUser.wipeAllUser()
        return employeeDB.model.remove({})
            .then(() => {
                return employeeDB.createNewEmployee(emp1);
            })
            .then(() => {
                return employeeDB.createNewEmployee(emp2);
            })
    });

    // find all end point
    it("Should return 2 entries when query for all", done => {
        return request(server.app)
            .get("/employee")
            .expect(200)
            .expect(rsp => {
                expect(rsp.body.length).toBe(2);
            })
            .end(done());
    });

    // update end point
    test("Should change the Name of Tom to Tommy, age to 34", done => {
        return request(server.app)
            //{ id:1, name: "Tommy", age:34}
            .put("/employees/1", )
            .send({ id: 1, name: "Tommy", age: 34 })
            .expect(200)
            .expect(rsp => {

                let tommy = rsp.body;
                console.log(tommy);
                expect(tommy.name).toBe("Tommy");
                expect(tommy.age).toBe(34);
            })
            .end(done());
    })

    // add new 
    it("Should add a new employee", done => {
        return request(server.app)
            .post("/employees")
            .send({
                id: 3,
                name: "John",
                age: 25
            })
            .expect(200)
            .expect(rsp => {
                expect(rsp.body.name).toBe("John");
            })
            .end(done());
    })

    // add new
    it("Should not add a new employee when name is missing", done => {
        return request(server.app)
            .post("/employees")
            .send({
                id: 4,
                age: 25
            })
            .expect(403)
            .end(done());
    })

    // delete
    it("should delete an employee", done => {
        return request(server.app)
            .delete("/employees/1")
            .expect(200)
            .expect(rsp => {
                expect(rsp.body.name).toBe("Tom");
            })
            .then(()=>{
                return employeeDB.findAllEmployee()
            })
            .then(rsp=>{
                expect(rsp.length).toBe(1);
                done();
            })
    })
});


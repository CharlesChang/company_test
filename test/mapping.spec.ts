import { employeeDB } from '../src/modules/mongodb/mongodb.module';
import { departmentDB } from '../src/modules/mongodb/mongodb.module';
const request = require("supertest");
import { server } from "../src/index";
describe("Mapping between Dep & Emp", () => {
  const dp1 = {
    id: 101,
    name: "IT"
  }
  const dp2 = {
    id: 100,
    name: "Marketing"
  }

  const emp1 = {
    id: 1,
    name: "Tom",
    age: 33
  }

  const emp2 = {
    id: 2,
    name: "Siri",
    age: 28
  }
  beforeEach(() => {
    return departmentDB.model.remove({})
      .then(() => {
        return departmentDB.createNewDepartment(dp1);
      })
      .then(() => {
        return departmentDB.createNewDepartment(dp2);
      })
      .then(() => {
        return employeeDB.createNewEmployee(emp1);
      })
      .then(() => {
        return employeeDB.createNewEmployee(emp2);
      })
  })

  //add employee to department
  it("should add Tom to IT department", done => {
    return request(server.app)
      .post("/departments/101/employees/1")
      .expect(200)
      .expect(rsp => {
        expect(rsp.body.department).toBe(101);
      })
      .then(() => {
        return employeeDB.findEmployeeById(1)
      })
      .then(rsp => {
        expect(rsp.department).toBe(101)
        done();
      })
  })

  //get all employee in a department
  it("should get all employees in a given department", async done => {
    await employeeDB.createNewEmployee({
      id: 4,
      name: "John",
      age: 67,
      department: 101
    });
    await employeeDB.findOneAndUpdate({
      id: 1,
      name: "Tom",
      age: 33,
      department: 101
    });
    return request(server.app)
      .get("deparments/101/employees")
      .expect(200)
      .expect(rsp => {
        expect(rsp.body.length).toBe(2);
      })
      .end(done());
  })
})


import { ObjectID } from "mongodb";
import { Document, Schema } from "mongoose";

export type DataBaseName = "Department" | "Employee";

// this is the model we write to database


export interface dbRspError {
    errors: any
    message: string
}
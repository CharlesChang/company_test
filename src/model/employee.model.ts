export interface EmployeeData{
  id:number,
  name:string,
  age:number,
  department?:number
}
import { departmentDB } from '../mongodb/mongodb.module';
import { Application, Request, Response, NextFunction } from 'express';


export class DepartmentAPI {

    constructor(app: Application) {

        //find all
        app.get("/departments", (req, rsp) => {
            departmentDB.findAllDepartment().then(
                data => {
                    rsp.status(200)
                        .send(data);
                },
                err => {
                    rsp.status(403).send(err);
                }
            )
        });

        //find by id
        app.get('/departments/:id', (req, rsp) => {
            console.log("id = ", req.params.id);
            departmentDB.findDepartmentById(req.params.id).then(
                data => {
                    rsp.status(200)
                        .send(data);
                },
                err => {
                    rsp.status(403)
                        .send(err);
                }
            )
        })


        //create
        app.post("/departments", (req, rsp) => {
            let { id, name } = req.body;

            departmentDB.createNewDepartment({ id, name })
                .then(newDep => {
                    rsp.status(200)
                        .send(newDep);
                }, err => {
                    rsp.status(403).send(err);
                })
        })

        //update
        app.put("/departments", (req, rsp) => {
            let { id, name } = req.body;

            departmentDB.findOneAndUpdate({ id, name })
                .then(newUser => {
                    rsp.status(200)
                        .send(newUser);
                }, err => {
                    rsp.status(403).send(err);
                })
        })

        //delete
        app.delete("/departments/:id", (req, rsp) => {
            departmentDB.findOneAndDelete(req.params.id).then(
                success => {
                    rsp.status(200).send(success);
                },
                err => {
                    rsp.status(403).send(err);
                }
            )
        })

        // your new routes here
    }

}

export const departmentAPI = (app: Application) => new DepartmentAPI(app);


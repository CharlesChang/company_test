import { departmentAPI } from './department.api';
import { employeeAPI } from './employee.api';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import { Application } from "express";

export class RouteCreator {
    constructor(app: Application) {
        // routing configuration
        app.use(cors());
        app.use(bodyParser.json());



        // attach all routes
        this.assembleRoutes(app);
    }
    
    private assembleRoutes(app: Application) {
        // Add more modularized routes
        employeeAPI(app);
        departmentAPI(app);
    }
}

export const routeCreator = (app: Application) => new RouteCreator(app);
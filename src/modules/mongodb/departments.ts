import { DepartmentData } from '../../model/department.model';
import { NextFunction } from 'express';
import { ObjectID } from "mongodb";
import { Schema, Model, Document, model, DocumentQuery } from "mongoose";
const logger = require('tracer').console();
import * as _ from "lodash";


export class Department {
    private DepartmentModel: Model<Document>;
    private schema: Schema;

    constructor() {
        this.schema = new Schema({
            id: {
                type: Number,
                required:true
            },
            name:{
                type:String,
                required:true
            },
        });

        // set up some models
        this.DepartmentModel = model("Department", this.schema);
    };

    /**
     * Try not to use the model directly since the type definition will be lost
     */
    public get model() {
        return this.DepartmentModel;
    }

    public findAllDepartment():Promise<DepartmentData[]>{
        return <any>this.DepartmentModel.find();
    }

    public findDepartmentById(id:number):Promise<DepartmentData>{
        return <any>this.DepartmentModel.findOne({id: id});
    }

    public createNewDepartment(dep:DepartmentData):Promise<DepartmentData>{

        let model = new this.DepartmentModel(dep);
        return <any>model.save();
    }

    public findOneAndUpdate(dep:DepartmentData):Promise<DepartmentData>{
        return new Promise( async(resolve, reject)=>{
            try{
                let doc = await this.DepartmentModel.findOneAndUpdate({
                    id: dep.id
                },{
                    $set:{
                        name:dep.name,
                    }
                },{
                    new: true
                })

                resolve(<any>doc);
            }catch(err){
                reject(err)
            }
        })
    }

    public findOneAndDelete(id:number):Promise<DepartmentData>{
        return <any>this.DepartmentModel.findOneAndRemove({id:id});
    }

    

}


export const _department = new Department();
import { EmployeeData } from '../../model/employee.model';
import { NextFunction } from 'express';
import { ObjectID } from "mongodb";
import { Schema, Model, Document, model, DocumentQuery } from "mongoose";
const logger = require('tracer').console();
import { isEmail } from "validator";
import { dbRspError } from "../../model/database.model";
import * as _ from "lodash";


export class Employee {
    private EmployeeModel: Model<Document>;
    private schema: Schema;

    constructor() {
        this.schema = new Schema({
            id: {
                type: Number,
                required:[true, "id is required"]
            },
            name:{
                type:String,
                required:[true, "name is required"]
            },
            age:{
                type:Number,
                required:[true, "age is required"]
            },
            department:{
                type:Number
            }

        });

        // set up some models
        this.EmployeeModel = model("Employee", this.schema);
    };

    /**
     * Try not to use the model directly since the type definition will be lost
     */
    public get model() {
        return this.EmployeeModel;
    }

    public findAllEmployee():Promise<EmployeeData[]>{
        return <any>this.EmployeeModel.find();
    }

    public findEmployeeById(id:number):Promise<EmployeeData>{
        return <any>this.EmployeeModel.findOne({id: id});
    }

    public createNewEmployee(user:EmployeeData):Promise<EmployeeData>{

        let model = new this.EmployeeModel(user);
        return <any>model.save();
    }

    public findOneAndUpdate(user:EmployeeData):Promise<EmployeeData>{
        return new Promise( async(resolve, reject)=>{
            try{
                let doc = await this.EmployeeModel.findOneAndUpdate({
                    id: user.id
                },{
                    $set:{
                        name:user.name,
                        age:user.age
                    }
                },{
                    new: true
                })

                resolve(<any>doc);
            }catch(err){
                reject(err)
            }
        })
    }

    public findOneAndDelete(id:number):Promise<EmployeeData>{
        return <any>this.EmployeeModel.findOneAndRemove({id:id});
    }


    public findEmployeeByDepartmentId(dpid:number):Promise<EmployeeData[]>{
        return <any>this.EmployeeModel.find({department:dpid});
    }

    public addToDepartment(dpid:number, epid:number):Promise<EmployeeData>{
        return new Promise(async(resolve, reject)=>{
            try{
                let doc = await this.EmployeeModel.findOneAndUpdate({
                    id: epid,
                },{
                    $set:{
                        department:dpid
                    }
                },{
                    new: true
                })

                resolve(<any>doc);
            }catch(err){
                reject(err);
            }
        })
    }


}


export const _employee = new Employee();
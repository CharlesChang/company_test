import { _department, Department } from './departments';
import { _employee, Employee } from './employees';
import { MongoClient, Db, MongoError, ObjectID } from "mongodb";
import mongoose = require("mongoose");
const logger = require('tracer').console();
mongoose.Promise = global.Promise; //using node default promise for mongoose



export class MongooseModule {
    private database = mongoose.connection;
    constructor() {
    }

    public bootstrap() {
        let me = this;
        mongoose.connect(process.env.MONGODB_URI, { useMongoClient: true });
        this.database.on('error', logger.error.bind(logger, 'connection error:'));
        this.database.once('open', () => {
            // we're connected!
            me.onCreateHook();
        });
    }

    private async onCreateHook() {
        //Your hook
    }

    public closeDatabase() {
        if (this.database) {
            this.database.close();
        } else {
            logger.log("Unable to close database: database was not initialized.")
        }
    }


}

export const mongooseDB = new MongooseModule();
export const employeeDB = _employee;
export const departmentDB = _department;
/* utilutil */
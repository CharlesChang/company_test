import { Pool, Client } from 'pg'


export class PostgresModule {
  private client: Client;

  public async boostrap() {
    this.client = new Client({
      connectionString: process.env.POSTGRES_URI
    })
    try {


      // const pool = new Pool({
      //   connectionString: process.env.POSTGRES_URI
      // })

      // await pool.query('SELECT NOW()', (err, res) => {
      //   console.log(err, res)
      //   pool.end()
      // })


      await this.client.connect()

      this.client.query('SELECT NOW()', (err, res) => {
        console.log(err, res)
        this.client.end()
      })
    } catch (err) {
      console.log("there is a problem")
      console.log(err)
    }
  }
}

export const postgresDB = new PostgresModule();
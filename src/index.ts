import { postgresDB } from './modules/postgresql/postgres.module';
import { routeCreator } from './modules/routes/route.module';
/* THIS CONFIGURATION STAYS TOP */
require("./envConfig");
/* END OF ENV CONFIGURATION */


import * as express from 'express';
import { mongooseDB  } from "./modules/mongodb/mongodb.module";
import * as portscanner from "portscanner";
const logger = require('tracer').console();


export class Server {
    private readonly PORT:number = Number(process.env.PORT);
    private _app: express.Application;

    constructor() {
        this.bootstrap();
        this.execute();
    }
    public get app() {
        return this._app;
    }

    private bootstrap() {
        /*Put all your startup hooks here*/
        
        this._app = express();
        postgresDB.boostrap();

        // find available port
        portscanner.checkPortStatus(this.PORT).then((status:any) => {
            if (status === "closed") {
                // port available case
                this._app.listen(this.PORT);
                console.log('\x1b[34m%s\x1b[0m', `Listening on port ${this.PORT}`);
            } else {
                // port not available case
                portscanner.findAPortNotInUse(3000).then(openPort => {
                    this._app.listen(openPort);
                    console.log('\x1b[34m%s\x1b[0m', `Listening on port ${openPort}`);
                });
            }
        })
    }


    private execute() {
        /* Put all your executions here*/


        // extract all routes into a decoupled module
        routeCreator(this._app);
    }


}



export const server = new Server();